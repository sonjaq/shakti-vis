var gulp = require('gulp');
var babel = require('gulp-babel');
var webserver = require('gulp-webserver');
var print = require('gulp-print');

gulp.task('js', function() {
  return gulp.src('app/js/*.js')               // #1. select all js files in the app folder
		.pipe(print())                         // #2. print each file in the stream
		.pipe(babel({ presets: ['es2015'] }))    // #3. transpile ES2015 to ES5 using ES2015 preset
		.pipe(gulp.dest('build'));               // #4. copy the results to the build folder
});

gulp.task('css', function() {
	return gulp.src([
			'node_modules/vis/dist/vis.css',
			'node_modules/metrics-graphics-2.11.0/dist/metricsgraphics.css'
		])
		.pipe(print())
		.pipe(gulp.dest('build/css'))
})

gulp.task('data', function() {
	return gulp.src([
			'app/data/*.json'
		])
		.pipe(print())
		.pipe(gulp.dest('build/data'))

})

gulp.task('libs', function() {
	return gulp.src([
			'node_modules/systemjs/dist/system.js',
			'node_modules/babel-polyfill/dist/polyfill.js',
			'node_modules/vis/dist/vis.js',
			'node_modules/d3/build/d3.js',
			'node_modules/metrics-graphics-2.11.0/dist/metricsgraphics.js',
			'node_modules/jquery/dist/jquery.js',
			'node_modules/react/dist/react.js'
		])
		.pipe(print())
		.pipe(gulp.dest('build/libs'));
});

gulp.task('build', ['js', 'css', 'libs', 'data'], function() {
	return gulp.src(['app/**/*.html', 'app/**/*.css', 'app/**/*.js'])
			.pipe(print())
			.pipe(gulp.dest('build'));
});

gulp.task('serve', ['build'], function () {
	gulp.src('build')
		.pipe(webserver({
			livereload: true,
			open: true,
			// fallback: 'index.html'
		})
	);
});

