// Global Available Vars

var omdbUrl = 'http://www.omdbapi.com/?y=&plot=short&r=json&t=';

var json1 = 'data/andie.json',
	json2 = 'data/leaf.json';
	json3 = 'data/erica.json';

var tooltip = d3.select("body")
	.append("div")
	.attr("class", "d3-tip")
	.style("display", "none");
	// .style('position', 'inherit');

var svg = d3.select('svg'),
	margin = {top:20, right: 40, bottom: 60, left: 40},
	width = svg.attr('width') - margin.left - margin.right
	height = svg.attr('height') - margin.top - margin.bottom;


	// Establish, configure the X Axis
var x = d3.scaleTime()
		.range([0,width])

var y = d3.scaleLinear()
		.range([height, 0]);


var allNetflixData = new Array;

function updateData1() { 
	removeGraph();
	d3.json(json1, drawGraph);
}

function updateData2() {
	removeGraph();
	d3.json(json2, drawGraph);
}

function updateData3() {
	removeGraph();
	d3.json(json3, drawGraph);
}

function removeGraph() {
	d3.selectAll( 'svg > *')
		.transition()
		.ease(d3.easeSin)
		.duration(300)
		.style('transform', 'scale(0.1)')
		.remove();
}

function populateAllData(d) {
	console.log(d, allNetflixData);
	// console.log('netflix', allNetflixData, 'd',d);
	// var results = allNetflixData.concat(d);
	Array.prototype.push.apply(allNetflixData, d);
}


function setDomains(data) {
	// Instead of calling d3.min/max, use extent, and a previously declared 
	// function.
	// 
	// x.domain([
	// 	d3.min(data, function(d) { return d.start; }), 
	// 	d3.max(data, function(d) { return d.start; })
	// ]);
	x.domain(d3.extent(data, getStart));
	y.domain([0,d3.max(data, function(d) { return d.count; })] );
}

// Used with extent as a helper function
function getStart(d) {
	return d.start;
}

function drawGraph(data) {

	var g = svg.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	// console.log(data);
	// if (error) throw error;
	filteredData  =  filterToSeriesAndEpisodes(data);
	entries = filterSeriesEpisodesUsingD3Nest(filteredData);

	// console.log(filteredData);
	setDomains(entries);

	// Establish axis ranges!
	
	g.append("g")
		.attr("class", "axis axis--x")
		.attr("transform", "translate(0," + height + ")")
		.call(d3.axisBottom(x))
		.selectAll("text")
		.attr("y",0)
		.attr("x",9)
		.attr("dy", ".35em")
		.attr("transform", "rotate(70)")
		.style("text-anchor", "start");

	g.append('g')
		.attr('class', 'axis axis--y')
		.call(d3.axisLeft(y));

	g.selectAll('circle')
		.data(entries).enter()
		.append('circle')
		.attr('cy', function(d, i) { return y(d.count);} )
		.attr('cx', function(d,i) { return x(d.start);})
		.attr('r', getRadius )
		.attr('fill', 
			function(d,i) { 
				return d3.interpolateRainbow(Math.random());
			} 
		)
		.on('mousemove', function(d) {
			var image;

			getImage(d, image);

			d3.select(d3.event.target)
				.transition()
				.ease(d3.easeBounceOut)
				.duration(600)
				.attr('r', d.count + getRadius(d));

			tooltip
				// .transition().ease(d3.easeBounceOut).duration(1000)
				.style('display', 'inline-block')
				.style('left', ((d3.event.pageX - 50) < 0) ? 25 : (d3.event.pageX - 50) + "px")
				.style('top',  d3.event.pageY + "px")
				.style('background-image', 'url(' + d.image + ')')
				.style('background-position', '50% 50%')
				.style('transform', 'scale(1.5)')
				// .style('filter', 'hue-rotate(220deg) saturate(1)')
				// .style('filter', 'filter: blur(20px) grayscale(20%)')
				.html(getInfoHtml(d))
		})
		.on('mouseout', 
			function(d) { 
				tooltip.style('display', 'none')
					// .style('transform', 'scale(1)');
				d3.select(d3.event.target)
					.transition()
					.ease(d3.easeSin)
					.duration(200)
					.attr('r', getRadius );
			}
		);

}

function getRadius(d) {
	return (d.count/10) + 5;
}


function getInfoHtml(d) {
	seriesOrMovie  =  d.count > 1 ? 'Series: ' : '';
	return '<strong>' + seriesOrMovie + d.name + '</strong>'
		+ '<pre>Watched ' + d.count +' time' + ((d.count) > 1 ? 's' : '')
		+ ' over ' + daysBetween(d) + ' day' + (daysBetween(d) > 1 ? 's' : '') + '</pre>'
		+ '<pre>Started Watching: ' + d.start.toDateString() + '</pre>'
		+ '<pre>Last Watched:  ' + d.end.toDateString() + '</pre>';
}

function getImage(item, image) {

	if ( item.image ) { 
		image = item.image; 
		return item; 
	}

	// Handle Marvel titles
	var searchTitle  =  item.name;

	if (searchTitle.match('Marvel')) {
		parts = searchTitle.split(/\s/);
		parts.shift();
		searchTitle = parts.join(' ');
	}

	fetch(omdbUrl + encodeURI(searchTitle))
		.then(function(response) {
			json = response.json();
			return json;
		})
		.then((jsonResponse) => {
			item.image = jsonResponse.Poster;
			image = item.image;
			return image;
		})
	
	
} 


function filterSeriesEpisodesUsingD3Nest(data) {
	var entries = d3.nest()
		// .key(function(d) { return d.count; } )
		// .key(function(d) { return daysBetween(d.start, d.end)})
		.entries(data);
	return entries;
}

function daysBetween(datum) {
	result = Math.round((datum.end - datum.start)/(1000*60*60*24));
	if (result < 1) return 1;
	return result;
}

function filterToSeriesAndEpisodes(data) {
	results  =  new Array;
	seriesIndices  =  new Array;

	data.forEach( function(item, index, array) {
		viewing = {
			name: item.title,
			start: new Date( item.date ),
			end: new Date( item.date ),
			completed: getCompleted(item)
		};

		if ( item.hasOwnProperty('seriesTitle') ) {
			if ( seriesIndices[item.seriesTitle] !== undefined ) {

				// Grab the index from the seriesIdices look up to grab the parent
				idx  =  seriesIndices[item.seriesTitle];
				parent  =  results[idx];

				// add child to parent, increment parent count
				parent.children.push(viewing);
				parent.count++;

				if ( viewing.end > parent.end ) {
					parent.end = viewing.end;
				}

				if ( viewing.start < parent.start) {
					parent.start = viewing.start;
				}
				return;

			}
			else {
				series = {
					name: item.seriesTitle,
					children: [],
					count: 1,
					start: new Date( item.date ),
					end: new Date( item.date ),
					image: null
				}

				results.push(series);
				
				idx = results.indexOf(series);
				seriesIndices[item.seriesTitle] = idx;
				
				results[idx].children.push(viewing);
				return;
			}
		}
		else {
			viewing.count = 1;
			results.push(viewing);
			return;
		}
	});

	// sortByDate(results);
	return results;
}

function sortByDate( data ) {
	data2 = data.sort( function(a,b) { 
		return new Date(a.start) - new Date(b.start);
	});
}


function getCompleted(item) {
	return ((item.bookmark/item.duration) > 0.9) ? true : false;
}



function filterToTitleAndSeries( data ) {
	return data.map( function(e) {

		return {
			'date': new Date(e.date),
			'value': e.seriesTitle ? e.seriesTitle : e.title 
		};
	});
}


function update(data) {

  // DATA JOIN
  // Join new data with old elements, if any.
  var circle = g.selectAll("circle")
	.data(data);

  setDomains(data);

  // UPDATE
  // Update old elements as needed.
  circle.attr("class", "update");

  // ENTER
  // Create new elements as needed.
  //
  // ENTER + UPDATE
  // After merging the entered elements with the update selection,
  // apply operations to both.
  circle.enter().append("circle")
	  .attr("class", "enter")
	  .attr("x", function(d, i) { return i * 32; })
	  .attr("dy", ".35em")
	.merge(circle)
	  .circle(function(d) { return d; });

  // EXIT
  // Remove old elements as needed.
  circle.exit().remove();
}

