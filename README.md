# README #

Install node, npm and gulp.

```
$ npm install
$ gulp build
$ gulp serve
```

If you leave gulp serve in one tab, and then run `gulp build`, changes will be deployed.
